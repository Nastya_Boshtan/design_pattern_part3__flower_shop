package com.epam.enums;

public enum Accessories {
  CONFETTI("Confetti",10),POSTCARD("Postcard",15), WOOD_STICKERS("WoodStickers",10);
  String name;
  Double price;

  Accessories (String name, double price){}

  public String getName(String name){return name;}

  public void setName(String name){
    this.name=name;
  }

  public Double getPrice(){return price;}

  public void setPrice(double price){
    this.price=price;
  }
}
