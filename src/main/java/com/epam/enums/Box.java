package com.epam.enums;

public enum Box {
  SMALL("Small",10),MEDIUM("Medium",15),BIG("Big",10);
  String name;
  Double price;

  Box (String name, double price){
    this.price = price;
    this.name = name;
  }

  public String getName(String name){return name;}

  public void setName(String name){
    this.name=name;
  }

  public Double getPrice(){return price;}

  public void setPrice(double price){
    this.price=price;
  }
}
