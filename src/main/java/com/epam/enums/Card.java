package com.epam.enums;

public enum Card {
  GOLDEN("Golden",5,7),SILVER("Silver",3,3);
  String name;
  double discount;
  double discountDelivery;

  Card(String name, double discount, double discountDelivery) {
    this.discount = discount;
    this.name = name;
    this.discountDelivery = discountDelivery;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getDiscount() {
    return discount;
  }

  public void setDiscount(double discount) {
    this.discount = discount;
  }

  public double getDiscountDelivery() {
    return discountDelivery;
  }

  public void setDiscountDelivery(double discountDelivery) {
    this.discountDelivery = discountDelivery;
  }
}
