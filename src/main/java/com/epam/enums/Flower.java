package com.epam.enums;

public enum Flower {
  ROSE("Rose", 21.5),
  GERBERA("Gerbera", 12),
  CHAMOMILE("Chamomile", 15),
  TULIP("Tulip", 18);

  String name;
  double price;

  Flower(String flowerName, double price) {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}

