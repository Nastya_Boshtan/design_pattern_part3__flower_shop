package com.epam.enums;

public enum Packaging {

  PAPER("Paper",10),BUCKRAM("Buckram",15);
  String name;
  Double price;

  Packaging (String name, double price){
    this.name = name;
    this.price = price;
  }

  public String getName(String name){return name;}

  public void setName(String name){
    this.name=name;
  }

  public Double getPrice(){return price;}

  public void setPrice(double price){
    this.price=price;
  }
}
