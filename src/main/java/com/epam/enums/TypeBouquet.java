package com.epam.enums;

public enum TypeBouquet {
    VALENTINE_BOUQUET("Valentines_Bouquet"), WEDDING_BOUQUET("Wedding_Bouquet"), FUNERAL_BOUQUET("Funeral_Bouquet"), CUSTOM_BOUQUET("Custom");
    String name;

    TypeBouquet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
