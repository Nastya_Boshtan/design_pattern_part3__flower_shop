package com.epam.factory;

import com.epam.enums.Accessories;
import com.epam.enums.Box;
import com.epam.enums.Packaging;
import com.epam.enums.Ribbon;

public interface BouquetFactory {
  Accessories getAccessory();
  Box getBox();
  Ribbon getRibbon();
  Packaging getPackaging();

}
