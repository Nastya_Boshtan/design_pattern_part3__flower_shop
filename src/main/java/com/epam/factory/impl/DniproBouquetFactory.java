package com.epam.factory.impl;

import com.epam.enums.Accessories;
import com.epam.enums.Box;
import com.epam.enums.Packaging;
import com.epam.enums.Ribbon;
import com.epam.factory.BouquetFactory;

public class DniproBouquetFactory implements BouquetFactory {
  @Override
  public Packaging getPackaging() {
    return Packaging.PAPER;
  }

  @Override
  public Ribbon getRibbon() {
    return Ribbon.PAPER;
  }

  @Override
  public Box getBox() {
    return Box.SMALL;
  }

  @Override
  public Accessories getAccessory() {
    return Accessories.POSTCARD;
  }
}
