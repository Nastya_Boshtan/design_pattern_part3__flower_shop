package com.epam.factory.impl;

import com.epam.enums.Accessories;
import com.epam.enums.Box;
import com.epam.enums.Packaging;
import com.epam.enums.Ribbon;
import com.epam.factory.BouquetFactory;

public class KyivBouquetFactory implements BouquetFactory {
  @Override
  public Packaging getPackaging() {
    return Packaging.BUCKRAM;
  }

  @Override
  public Ribbon getRibbon() {
    return Ribbon.SATIN;
  }

  @Override
  public Box getBox() {
    return Box.MEDIUM;
  }

  @Override
  public Accessories getAccessory() {
    return Accessories.WOOD_STICKERS;
  }

}
