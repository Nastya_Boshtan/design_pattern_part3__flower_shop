package com.epam.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

public class AccessoriesMenu {
    private Map<String, String> bouquetRange;
    private Map<String, Printable> bouquetMenu;
    private static Logger log = LogManager.getLogger(AccessoriesMenu.class);

    public AccessoriesMenu() {
        bouquetRange = new LinkedHashMap<>();
        bouquetRange.put("1", "1 - Confetti");
        bouquetRange.put("2", "2 - Postcard");
        bouquetRange.put("3", "3 - Wood stickers");
        bouquetRange.put("Q", "Q - Exit\n");

        bouquetMenu = new LinkedHashMap<>();
        bouquetMenu.put("1", this::getConfetti);
        bouquetMenu.put("2", this::getPostcard);
        bouquetMenu.put("3", this::getWoodSticker);
    }

    private void getConfetti() {

    }

    private void getPostcard() {

    }

    private void getWoodSticker() {

    }

    private void getCustomBouquet() {

    }

    private void showBouquets() {
        log.info("\nSelect a bouquet:");
        for (String str : bouquetRange.values()) {
            log.info(str);
        }
    }
}
