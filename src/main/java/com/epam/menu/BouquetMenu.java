package com.epam.menu;

import com.epam.enums.TypeBouquet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class BouquetMenu {
    protected static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(BouquetMenu.class);
    private Map<String, String> bouquetRange;
    private Map<String, Printable> bouquetMenu;

    public BouquetMenu() {
        bouquetRange = new LinkedHashMap<>();
        for (int i = 0; i < TypeBouquet.values().length; i++) {
            bouquetRange.put(String.valueOf(i + 1), TypeBouquet.values()[i].toString());
        }
       bouquetRange.put("Q", "Q - Deliver to client\n");

        bouquetMenu = new LinkedHashMap<>();
    }

    public static void main(String[] args) {
        new BouquetMenu().show();
    }


    private static void createBouquet() {
        log.info("\nSelect a bouquet:");

        TypeBouquet typeBouquet = null;
        try {
            typeBouquet = TypeBouquet.values()[Integer.parseInt(input.nextLine().trim()) - 1];
        } catch (NumberFormatException e) {
            DeliveryMenu.show();
        } finally {
        }
        ClientMenu.currentBouquet = ClientMenu.currentClient.getOrder().addBouquetToOrder(typeBouquet);
    }


    public static void show() {
        String keyMenu;
        do {
            new BouquetMenu().getBouquets();
            createBouquet();
            log.info(ClientMenu.currentBouquet);
            new DecoratorMenu().show();
            keyMenu = input.nextLine().toUpperCase().trim();
        } while (!keyMenu.equals("Q"));
    }


    public void getBouquets() {
        for (Map.Entry<String, String> pair : bouquetRange.entrySet()) {
            log.info(String.format("%s. %s", pair.getKey(), pair.getValue()));
        }
    }
}