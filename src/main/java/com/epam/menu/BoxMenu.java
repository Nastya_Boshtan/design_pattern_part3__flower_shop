package com.epam.menu;

import com.epam.enums.Box;
import com.epam.model.Client;
import com.epam.model.bouquet.Bouquet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BoxMenu {

  protected static Scanner input = new Scanner(System.in);
  private static Logger log = LogManager.getLogger(BoxMenu.class);
  protected Bouquet currentBouquet;
  protected Client currentClient;
  private Map<String, String> boxRange;
  private Map<String, Printable> boxMenu;

  public BoxMenu() {
    boxRange = new LinkedHashMap<>();
    boxRange.put("1", "1 - Small Box");
    boxRange.put("2", "2 - Medium Box");
    boxRange.put("3", "3 - Big Box");
    boxRange.put("Q", "Q - Exit\n");

    boxMenu = new LinkedHashMap<>();
    boxMenu.put("1", this::press1);
    boxMenu.put("2", this::press2);
    boxMenu.put("3", this::press3);
  }


  public void getBoxes() {
    for (String line : boxRange.values()) {
      log.info(line);
    }
  }

  public void press1() {
    currentBouquet.setBox(Box.SMALL);
  }

  public void press2() {
    currentBouquet.setBox(Box.MEDIUM);
  }

  public void press3() {
    currentBouquet.setBox(Box.BIG);
  }
  private void outputMenu() {
    log.info("\nMENU:");
    for (String str : boxRange.values()) {
      log.info(str);
    }
  }

  public void show() {
    String keyMenu;

      log.info("\n Please, choose a box from the list: ");
      outputMenu();
      keyMenu = input.nextLine().toUpperCase();
      try {
        boxMenu.get(keyMenu).print();
      } catch (Exception e) {
      }

  }
  public static void main(String[] args) {
    new BoxMenu().show();

  }
}
