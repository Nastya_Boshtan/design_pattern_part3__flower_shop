package com.epam.menu;

import com.epam.enums.TypeBouquet;
import com.epam.model.Client;
import com.epam.model.bouquet.Bouquet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class ClientMenu {
    protected static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(ClientMenu.class);
    protected static Bouquet currentBouquet;
    protected static Client currentClient;

    public ClientMenu() {
    }

    public static void main(String[] args) {
        new ClientMenu().show();
    }

    private static void chooseCurrentClient() {
        int clientNumber = Integer.parseInt(input.nextLine().trim());
        if ((clientNumber <= Client.getClients().size()) && (clientNumber > 0)) {
            currentClient = Client.getClients().get(clientNumber - 1);
            log.info(currentClient);
        } else {
            log.warn("Wrong input");
        }
    }

    private static void displayClients() {
        log.info("Please, choose client's number");
        for (int i = 0; i < Client.getClients().size(); i++) {
            log.info((i + 1) + ". " + Client.getClients().get(i).toString());
        }
    }

    private void createBouquet() {
        log.info("\nSelect a bouquet:");
        TypeBouquet typeBouquet = TypeBouquet.values()[Integer.parseInt(input.nextLine().trim())];
        currentBouquet = currentClient.getOrder().addBouquetToOrder(typeBouquet);
    }

    public static void show() {
        String keyMenu;
        do {
            displayClients();
            chooseCurrentClient();
            BouquetMenu.show();
            keyMenu = input.nextLine().toUpperCase().trim();
        } while (!keyMenu.equals("Q"));
    }
}
