package com.epam.menu;


import com.epam.enums.Accessories;
import com.epam.enums.Flower;
import com.epam.enums.Ribbon;
import com.epam.model.Client;
import com.epam.model.decorator.BouquetDecorator;
import com.epam.model.decorator.impl.AccessoryDecorator;
import com.epam.model.decorator.impl.FlowerDecorator;
import com.epam.model.decorator.impl.RibbonDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DecoratorMenu {
    protected static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(DecoratorMenu.class);
    private static BouquetDecorator currentDecorator;
    private Map<String, String> decoratorRange;
    private Map<Integer, Printable> decoratorMenu;

    public DecoratorMenu() {
        decoratorRange = new LinkedHashMap<>();
        decoratorRange.put("1", "1 - Flower decorator");
        decoratorRange.put("2", "2 - Accessory decorator");
        decoratorRange.put("3", "3 - Ribbon decorator");
        decoratorRange.put("Q", "Q - Exit\n");

        decoratorMenu = new LinkedHashMap<>();
        decoratorMenu.put(1, this::press1);
        decoratorMenu.put(2, this::press2);
        decoratorMenu.put(3, this::press3);
    }

    public static void show() {
        String keyMenu;
        DecoratorMenu menu = new DecoratorMenu();
        do {
            menu.getDecorators();
            menu.createDecorator();
            log.info(DecoratorMenu.currentDecorator);
            keyMenu = input.nextLine().toUpperCase().trim();
        } while (!keyMenu.equals("Q"));
        ClientMenu.currentClient.getOrder().addOneBouquet(ClientMenu.currentBouquet);
        BouquetMenu.show();
    }

    private void press3() {
        currentDecorator = new RibbonDecorator(ClientMenu.currentBouquet);
        log.info("Please choose ribbon");
        for (Ribbon ribbon : Ribbon.values()) {
            log.info(ribbon);
        }
        currentDecorator.setRibbon(Ribbon.values()[Integer.parseInt(input.nextLine().trim()) - 1]);
    }

    private void press2() {
        currentDecorator = new AccessoryDecorator(ClientMenu.currentBouquet);
        log.info("Please choose accesory");
        for (Accessories accessories : Accessories.values()) {
            log.info(accessories);
        }
        currentDecorator.addAccessory(Accessories.values()[Integer.parseInt(input.nextLine().trim()) - 1]);
    }

    private void press1() {
        currentDecorator = new FlowerDecorator(ClientMenu.currentBouquet);
        log.info("Please choose Flower");
        for (Flower flower : Flower.values()) {
            log.info(flower);
        }
        currentDecorator.addFlower(Flower.values()[Integer.parseInt(input.nextLine().trim()) - 1]);
    }

    private void createDecorator() {
        log.info("\nSelect a decorator:");
        int decoratorNumber = Integer.parseInt(input.nextLine().trim());
        decoratorMenu.get(decoratorNumber).print();
    }

    public void getDecorators() {
        for (Map.Entry<String, String> pair : decoratorRange.entrySet()) {
            log.info(String.format("%s. %s", pair.getKey(), pair.getValue()));
        }
    }

}
