package com.epam.menu;

import com.epam.model.action.ActionFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DeliveryMenu {
    protected static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(DeliveryMenu.class);
    private Map<String, String> deliveryRange;
    private Map<Integer, Printable> deliveryMenu;

    public DeliveryMenu() {
        deliveryRange = new LinkedHashMap<>();
        deliveryRange.put("1", "1 - Approve order and deliver");
        deliveryRange.put("Q", "Q - Exit\n");

        deliveryMenu = new LinkedHashMap<>();

    }

    public void deliver() {
        ActionFacade actionFacade = new ActionFacade(ClientMenu.currentClient);
        actionFacade.deliver();
    }

    public static void show() {
        String keyMenu;
        DeliveryMenu menu = new DeliveryMenu();
        menu.deliver();
        do {
            keyMenu = input.nextLine().toUpperCase().trim();
        } while (!keyMenu.equals("Q"));
        ClientMenu.show();
    }

    private void createDelivery() {
        log.info("\nSelect a delivery:");
        int deliveryNumber = Integer.parseInt(input.nextLine().trim());
        deliveryMenu.get(deliveryNumber).print();
    }

    public void getDeliveries() {
        for (Map.Entry<String, String> pair : deliveryRange.entrySet()) {
            log.info(String.format("%s. %s", pair.getKey(), pair.getValue()));
        }
    }
}
