package com.epam.model;

import com.epam.enums.City;

public class Address {

  private City city;

  // another address fields todo add later


  public Address(City city) {
    this.city = city;
  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "Address{" +
            "city=" + city +
            '}';
  }
}
