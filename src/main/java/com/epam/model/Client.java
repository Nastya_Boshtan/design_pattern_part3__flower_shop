package com.epam.model;

import com.epam.enums.Card;
import com.epam.enums.City;
import java.util.Arrays;
import java.util.List;

public class Client {

  private static List<Client> clients;

  static {
    Client client1 = new Client("Sergo", new Address(City.LVIV), Card.SILVER);
    Client client2 = new Client("Nastia", new Address(City.LVIV), Card.SILVER);
    Client client3 = new Client("Andrii", new Address(City.LVIV), Card.GOLDEN);
    Client client4 = new Client("Ihor", new Address(City.LVIV), Card.GOLDEN);
    Client client5 = new Client("Roman", new Address(City.LVIV), Card.SILVER);
    clients = Arrays.asList(client1, client2, client3, client4, client5);
  }

  private String clientName;

  private Order order;

  private Address address;

  private Card card;

  public Client(String clientName, Address address, Card discountCard) {
    this.address = address;
    this.clientName = clientName;
    this.card = discountCard;
    this.order = new Order(address.getCity());
  }

  public static List<Client> getClients() {
    return clients;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Card getCard() {
    return card;
  }

  public void setCard(Card card) {
    if (card != null) {
      this.card = card;
      //todo can be added verification card discount - if discount is smaller then previous then not change
    }
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public void addClient(Client client) {
    clients.add(client);
  }
    @Override
    public String toString () {
      return "Client{" +
          "" + clientName +
          ", order=" + order +
          ", address=" + address +
          ", card=" + card +
          '}';
    }
  }


