package com.epam.model;

import com.epam.enums.City;
import com.epam.enums.TypeBouquet;
import com.epam.model.bouquet.Bouquet;
import com.epam.model.shop.Shop;
import com.epam.model.shop.impl.DniproShop;
import com.epam.model.shop.impl.KiyvShop;
import com.epam.model.shop.impl.LvivShop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private static final Logger log = LogManager.getLogger(Order.class);
    private List<Bouquet> bouquets = new ArrayList<>();
    private Shop shop;
    private City city;
    private double totalPrice;

    public Order(City city) {
        switch (city) {
            case LVIV:
                this.shop = new LvivShop();
                this.city = City.LVIV;
                break;
            case KYIV:
                this.shop = new KiyvShop();
                this.city = City.KYIV;
                break;
            case DNIPRO:
                this.shop = new DniproShop();
                this.city = City.DNIPRO;
                break;
            default:
        }
    }

    public void addOneBouquet(Bouquet bouquet) {
        bouquets.add(bouquet);
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public Bouquet addBouquetToOrder(TypeBouquet typeBouquet) {
        Bouquet bouquet = shop.orderBouquet(typeBouquet);
        if (bouquet != null) {
            bouquets.add(bouquet);
        }
        return bouquet;
    }

    public double calculateTotalPrice() {
        double totalPrice = 0;
        for (Bouquet bouquet : bouquets) {
            totalPrice += bouquet.getPrice();
        }
        this.totalPrice = totalPrice;
        return totalPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
