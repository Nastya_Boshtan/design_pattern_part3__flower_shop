package com.epam.model.action;

import com.epam.model.Client;
import com.epam.model.boxing.BoxSystem;
import com.epam.model.boxing.impl.Big;
import com.epam.model.boxing.impl.Medium;
import com.epam.model.boxing.impl.Small;
import com.epam.model.delivery.DeliverySystem;
import com.epam.model.delivery.impl.Glovo;
import com.epam.model.delivery.impl.Uber;
import com.epam.model.discount.DiscountSystem;
import com.epam.model.discount.GoldenDiscount;
import com.epam.model.discount.SilverDiscount;

public class ActionFacade {

    private DeliverySystem deliverySystem;
    private BoxSystem boxSystem;
    private DiscountSystem discontSystem;
    private Client currentClient;

    public ActionFacade(Client client) {
        this.currentClient = client;
        switch (client.getCard()) {
            case GOLDEN:
                discontSystem = new GoldenDiscount();
                break;
            case SILVER:
                discontSystem = new SilverDiscount();
                break;
            default:
                discontSystem = new DiscountSystem() {
                    @Override
                    public double getDiscount() {
                        return 0;
                    }

                    @Override
                    public double getDeliveryDiscount() {
                        return 0;
                    }
                };
        }
        if (client.getOrder().getBouquets().size() > 3) {
            boxSystem = new Big();
            deliverySystem = new Uber();
        } else if (client.getOrder().getBouquets().size() > 1) {
            boxSystem = new Medium();
            deliverySystem = new Glovo();
        } else {
            boxSystem = new Small();
            deliverySystem = new Glovo();
        }
    }

    public void deliver() {
        boxSystem.getBox();
        double boxingPrice = boxSystem.getBoxPrice() * discontSystem.getDiscount();
        //todo refactor distance initialization
        double deliveryPrice = deliverySystem.getPrice(100) * discontSystem.getDeliveryDiscount();
        currentClient.getOrder()
                .setTotalPrice(currentClient.getOrder().getTotalPrice() + boxingPrice + deliveryPrice);
        deliverySystem.deliver();
    }
}
