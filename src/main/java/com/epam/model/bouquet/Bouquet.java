package com.epam.model.bouquet;

import com.epam.enums.*;

import java.util.ArrayList;
import java.util.List;

public abstract class Bouquet {

    protected TypeBouquet typeBouquet;
    protected Box box;
    protected Packaging packaging;
    protected Ribbon ribbon;
    protected List<Flower> flowers = new ArrayList<>();
    protected List<Accessories> accessories = new ArrayList<>();
    double price = 0;

    public List<Flower> getFlowers() {
        return flowers;

    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    public List<Accessories> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessories> accessories) {
        this.accessories = accessories;
    }

    public void addAccessory(Accessories accessory) {
        accessories.add(accessory);
    }

    public Ribbon getRibbon() {
        return ribbon;
    }

    public void setRibbon(Ribbon ribbon) {
        this.ribbon = ribbon;
    }

    public TypeBouquet getTypeBouquet() {
        return typeBouquet;
    }

    public void setTypeBouquet(TypeBouquet typeBouquet) {
        this.typeBouquet = typeBouquet;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }

    public abstract void prepare();

    public double getPrice() {
        for (Flower flower : flowers) {
            if (flower == Flower.CHAMOMILE) {
                price += Flower.CHAMOMILE.getPrice();
            } else if (flower == Flower.GERBERA) {
                price += Flower.GERBERA.getPrice();
            } else if (flower == Flower.ROSE) {
                price += Flower.ROSE.getPrice();
            } else if (flower == Flower.TULIP) {
                price += Flower.TULIP.getPrice();
            }

        }
        for (Accessories accessory : accessories) {
            if (accessory == Accessories.CONFETTI) {
                price += Accessories.CONFETTI.getPrice();
            } else if (accessory == Accessories.POSTCARD) {
                price += Accessories.POSTCARD.getPrice();
            } else if (accessory == Accessories.WOOD_STICKERS) {
                price += Accessories.WOOD_STICKERS.getPrice();
            }
        }
        price += box.getPrice();
        price += packaging.getPrice();
        price += ribbon.getPrice();
        return price;
    }
}

