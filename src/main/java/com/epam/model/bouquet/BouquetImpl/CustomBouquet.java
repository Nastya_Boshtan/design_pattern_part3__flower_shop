package com.epam.model.bouquet.BouquetImpl;

import com.epam.enums.TypeBouquet;
import com.epam.factory.BouquetFactory;
import com.epam.model.bouquet.Bouquet;

public class CustomBouquet extends Bouquet {

  private BouquetFactory bouquetFactory;


  public CustomBouquet(BouquetFactory bouquetFactory) {
    this.bouquetFactory = bouquetFactory;
    this.typeBouquet = TypeBouquet.CUSTOM_BOUQUET;
  }

  @Override
  public void prepare() {
    this.packaging = bouquetFactory.getPackaging();
    this.accessories.add(bouquetFactory.getAccessory());
    this.ribbon = bouquetFactory.getRibbon();
  }

}
