package com.epam.model.bouquet.BouquetImpl;

import com.epam.enums.Flower;
import com.epam.enums.TypeBouquet;
import com.epam.factory.BouquetFactory;
import com.epam.model.bouquet.Bouquet;

public class FuneralBouquet  extends Bouquet {
  private BouquetFactory bouquetFactory;


  public FuneralBouquet(BouquetFactory bouquetFactory) {
    this.bouquetFactory = bouquetFactory;
    this.typeBouquet= TypeBouquet.FUNERAL_BOUQUET;
  }

  @Override
  public void prepare() {
    this.packaging=bouquetFactory.getPackaging();
    this.accessories.add(bouquetFactory.getAccessory());
    this.ribbon=bouquetFactory.getRibbon();
    this.flowers.add(Flower.ROSE);

  }

}
