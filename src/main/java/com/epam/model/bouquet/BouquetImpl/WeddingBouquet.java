package com.epam.model.bouquet.BouquetImpl;

import com.epam.enums.Flower;
import com.epam.enums.TypeBouquet;
import com.epam.factory.BouquetFactory;
import com.epam.model.bouquet.Bouquet;

public class WeddingBouquet extends Bouquet {
  private BouquetFactory bouquetFactory;

  public WeddingBouquet(BouquetFactory bouquetFactory) {
    this.bouquetFactory = bouquetFactory;
    this.typeBouquet= TypeBouquet.WEDDING_BOUQUET;
  }
  @Override
  public void prepare() {
    this.box=bouquetFactory.getBox();
    this.accessories.add(bouquetFactory.getAccessory());
    this.ribbon=bouquetFactory.getRibbon();
    this.flowers.add(Flower.TULIP);

  }

}
