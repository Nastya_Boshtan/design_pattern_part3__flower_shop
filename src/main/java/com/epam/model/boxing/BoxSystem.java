package com.epam.model.boxing;

public interface BoxSystem  {
  void getBox();
  double getBoxPrice();

}
