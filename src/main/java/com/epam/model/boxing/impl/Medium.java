package com.epam.model.boxing.impl;

import com.epam.enums.Box;
import com.epam.model.boxing.BoxSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Medium implements BoxSystem {
  Logger log = LogManager.getLogger(Medium.class);

  @Override
  public void getBox() {

  }

  @Override
  public double getBoxPrice() {
    log.info("Price of medium box packaging");
    return Box.MEDIUM.getPrice();
  }
}
