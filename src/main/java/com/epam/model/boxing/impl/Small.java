package com.epam.model.boxing.impl;

import com.epam.enums.Box;
import com.epam.model.boxing.BoxSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Small implements BoxSystem {
  Logger log = LogManager.getLogger(Small.class);

  @Override
  public void getBox() {

  }

  @Override
  public double getBoxPrice() {
    log.info("Price of small box packaging");
    return Box.SMALL.getPrice();
  }
}
