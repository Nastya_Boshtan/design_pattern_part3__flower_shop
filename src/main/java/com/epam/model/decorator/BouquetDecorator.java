package com.epam.model.decorator;

import com.epam.model.bouquet.Bouquet;

public abstract class BouquetDecorator extends Bouquet {
  protected Bouquet bouquet;

  public BouquetDecorator() {
  }

  public BouquetDecorator(Bouquet currentBouquet) {
    this.bouquet = currentBouquet;
  }
}
