package com.epam.model.decorator.impl;

import com.epam.enums.Accessories;
import com.epam.model.bouquet.Bouquet;
import com.epam.model.decorator.BouquetDecorator;

public class AccessoryDecorator extends BouquetDecorator {

    public AccessoryDecorator(Bouquet currentBouquet) {
        super(currentBouquet);
    }

    public void addAccessory(Accessories accessory) {
    bouquet.addAccessory(accessory);
  }

  @Override
  public void prepare() {

  }
}
