package com.epam.model.decorator.impl;

import com.epam.enums.Flower;
import com.epam.model.bouquet.Bouquet;
import com.epam.model.decorator.BouquetDecorator;

public class FlowerDecorator extends BouquetDecorator {

  public FlowerDecorator(Bouquet currentBouquet) {
    super(currentBouquet);
  }

  public void addFlower(Flower flower) {
    bouquet.addFlower(flower);
  }

  @Override
  public void prepare() {

  }

}
