package com.epam.model.decorator.impl;

import com.epam.enums.Ribbon;
import com.epam.model.bouquet.Bouquet;
import com.epam.model.decorator.BouquetDecorator;

public class RibbonDecorator extends BouquetDecorator {

  public RibbonDecorator(Bouquet currentBouquet) {
    super(currentBouquet);
  }

  public void setRibbon(Ribbon ribbon) {
    bouquet.setRibbon(ribbon);
  }

  @Override
  public void prepare() {

  }
}
