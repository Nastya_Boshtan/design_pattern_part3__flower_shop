package com.epam.model.delivery;

public interface DeliverySystem {
  double getPrice(double distance);
  void deliver();

}
