package com.epam.model.delivery.impl;

import com.epam.model.delivery.DeliverySystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Glovo implements DeliverySystem {
  Logger log = LogManager.getLogger(Glovo.class);

  private static final double multiplier=2.5;
  @Override
  public double getPrice(double distance) {
    log.info("Price of delivery by Glovo:");
    return distance*multiplier;
  }

  @Override
  public void deliver() {

  }
}
