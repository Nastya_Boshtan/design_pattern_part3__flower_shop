package com.epam.model.delivery.impl;

import com.epam.model.delivery.DeliverySystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Uber implements DeliverySystem {
  private static final double multiplier=2.1;
  Logger log = LogManager.getLogger(Uber.class);
  @Override
  public double getPrice(double distance)
  {
    log.info("Price of delivery by Uber:");
    return distance*multiplier;
  }

  @Override
  public void deliver() {

  }
}
