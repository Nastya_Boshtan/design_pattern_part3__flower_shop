package com.epam.model.discount;

public interface DiscountSystem {
double getDiscount();
double getDeliveryDiscount();
}
