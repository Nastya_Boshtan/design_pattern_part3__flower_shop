package com.epam.model.discount;

import com.epam.enums.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GoldenDiscount implements DiscountSystem {
  Logger log = LogManager.getLogger(GoldenDiscount.class);
  @Override
  public double getDiscount() {
    log.info("Discount for bouquet golden card owner");
    return Card.GOLDEN.getDiscount();
  }

  @Override
  public double getDeliveryDiscount() {
    log.info("Discount for delivery golden card owner");
    return Card.GOLDEN.getDiscountDelivery();
  }


}
