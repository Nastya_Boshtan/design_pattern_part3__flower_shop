package com.epam.model.discount;

import com.epam.enums.Card;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SilverDiscount implements DiscountSystem{
  Logger log = LogManager.getLogger(GoldenDiscount.class);
  @Override
  public double getDiscount() {
    log.info("Discount for bouquet silver card owner");
    return Card.SILVER.getDiscount();
  }

  @Override
  public double getDeliveryDiscount() {
    log.info("Discount for delivery silver card owner");
    return Card.SILVER.getDiscountDelivery();
  }

}
