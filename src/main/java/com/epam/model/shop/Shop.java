package com.epam.model.shop;

import com.epam.enums.TypeBouquet;
import com.epam.model.bouquet.Bouquet;

public abstract class Shop {
  protected abstract Bouquet getBouquet(TypeBouquet typeBouquet);

  public Bouquet orderBouquet(TypeBouquet typeBouquet) {
    Bouquet bouquet = getBouquet(typeBouquet);
    if (bouquet != null) {
      bouquet.prepare();

    }
    return bouquet;
  }



}
