package com.epam.model.shop.impl;

import com.epam.enums.TypeBouquet;
import com.epam.factory.BouquetFactory;
import com.epam.factory.impl.LvivBouquetFactory;
import com.epam.model.bouquet.Bouquet;
import com.epam.model.bouquet.BouquetImpl.FuneralBouquet;
import com.epam.model.bouquet.BouquetImpl.ValentineDayBouquet;
import com.epam.model.bouquet.BouquetImpl.WeddingBouquet;
import com.epam.model.shop.Shop;

public class LvivShop extends Shop {
  @Override
  protected Bouquet getBouquet(TypeBouquet typeBouquet) {
    Bouquet bouquet;
    BouquetFactory factory = new LvivBouquetFactory();
    switch (typeBouquet) {
      case VALENTINE_BOUQUET:
        bouquet = new ValentineDayBouquet(factory);
        break;
      case WEDDING_BOUQUET:
        bouquet = new WeddingBouquet(factory);
        break;
      case FUNERAL_BOUQUET:
        bouquet = new FuneralBouquet(factory);
        break;
      default:

        bouquet = null;
    }
    return bouquet;
  }

}
